<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 <!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<link rel="shortcut icon" href="images/logo.png" type="image/x-icon">
<title>Pustaka.com</title>
<style>
  body{
     background: url(images/background.png) no-repeat fixed;
     background-color:whitesmoke;
     background-size:cover;
    }

    #cont{
      margin-top: 200px;
    }

    #col{
    background-color:grey;
    opacity:0.8;
    filter:alpha(opacity=60); 
    color: white;
    font-size:20px;
    margin-top: 200;
    }

    
</style>
</head>
  <body>
  <!-- -------------------------content------------------------- -->
    <div class="container" id="cont">
      <div class="row justify-content-center ">
        <div class=" col-sm-5 col-lg-5 mt-3 border rounded p-3 text-center" id="col">
              <h2 class="p-3 text-center text-light">Pustaka.com </h2>
              <a href="data.php" type="button" class="btn btn-primary btn-lg ">Masuk</a>
        </div>
      </div>
    </div>

<!-- -------------------------footer------------------------- -->
    <div class="container justify-content-center fixed-bottom" >
      <div class="row ">
        <div class="col">
          <p class="text-center">Pustaka.com || Assessement Project || Khaniful Huda</p>
          <p class="text-center">&copy;2020</p>
        </div>
      </div>
    </div>
        
    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  </body>
</html>